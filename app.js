/* eslint-disable linebreak-style */
/* eslint-disable comma-dangle */
/* eslint-disable linebreak-style */
/* eslint-disable indent */
/* eslint-disable linebreak-style */
/* eslint-disable quotes */
/* eslint-disable linebreak-style */
/* eslint-disable require-jsdoc */
/* eslint-disable linebreak-style */
start();

function start() {
  const amountOfAudioToMove = getAmountOfAudioToMove();
  const delays = getDelaysForEachAction();
  const audioRowsLength = getAudioRowsLength();
  const startAudioNumber = audioRowsLength - 1;
  const endAudioNumber = audioRowsLength - amountOfAudioToMove;
  // eslint-disable-next-line max-len
  for (let audioNumber = startAudioNumber; audioNumber >= endAudioNumber; audioNumber--) {
    const amountOfAudioProcessed = startAudioNumber - audioNumber + 1;
    const timeout = delays.completeDelay * (amountOfAudioProcessed);
    const audioRow = getAudioRow(audioNumber);
    setTimeout(
      addAudio, timeout,
      audioRow, amountOfAudioToMove, amountOfAudioProcessed,
      delays
    );
  }
}

function addAudio(
  audioRow, amountOfAudioToMove, amountOfAudioProcessed,
  delays
) {
  setTimeout(
    selectAudio, delays.delayForFirstAction,
    audioRow
  );
  setTimeout(
    clickMoreButton, delays.delayForSecondAction,
    audioRow
  );
  setTimeout(
    clickAddToPlaylistButton, delays.delayForThirdAction,
    audioRow
  );
  setTimeout(
    finishAudioAddition, delays.delayForFourthAction,
    audioRow, amountOfAudioToMove, amountOfAudioProcessed,
    delays.completeDelay
  );
}

function selectAudio(audioRow) {
  audioRow.scrollIntoView(false);
  audioRow.onmouseover();
}

function clickMoreButton(audioRow) {
  const moreButton = getMoreButton(audioRow);
  moreButton.click();
}

function clickAddToPlaylistButton(audioRow) {
  const addToPlaylistButton = audioRow.querySelector(
    ".audio_row__more_action_add_to_playlist"
  );
  addToPlaylistButton.click();
}

function finishAudioAddition(
  audioRow, amountOfAudioToMove, amountOfAudioProcessed,
  completeDelay,
) {
  addAudioToPlaylist(audioRow);
  collapseAddToPlaylistWindow(audioRow);

  console.clear();

  const amountOfAudioLeft = amountOfAudioToMove - amountOfAudioProcessed;
  const timeLeftNotifier = getTimeLeftNotifier(
    amountOfAudioLeft, completeDelay
  );
  console.log(timeLeftNotifier);

  const completionNotifier = getCompletionNotifier(
    amountOfAudioToMove, amountOfAudioProcessed
  );
  console.log(completionNotifier);

  if (isProcessEnded(amountOfAudioLeft)) {
    showEndedProcessNotifier(amountOfAudioToMove);
  }
}

function getAudioRowsLength() {
  const audioRows = getAudioRows();
  const audioRowsLength = audioRows.length;
  return audioRowsLength;
}

function getAmountOfAudioToMove() {
  const amountOfAudioToMove = +prompt(
    "Введите количество аудио для добавления: ", "1000"
  );
  return amountOfAudioToMove;
}

function getAudioRows() {
  const itemsContainer = document.querySelector(
    ".CatalogBlock__itemsContainer"
  );
  const audioRows = itemsContainer.querySelectorAll(".audio_row");
  return audioRows;
}

function getDelaysForEachAction() {
  const inputDelay = +prompt("Введите значение задержки", "1000");
  const delayForFirstAction = inputDelay;
  const delayForSecondAction = inputDelay * 2;
  const delayForThirdAction = inputDelay * 3;
  const delayForFourthAction = inputDelay * 4;
  const completeDelay = delayForFourthAction + 100;
  const delays = {
    delayForFirstAction: delayForFirstAction,
    delayForSecondAction: delayForSecondAction,
    delayForThirdAction: delayForThirdAction,
    delayForFourthAction: delayForFourthAction,
    completeDelay: completeDelay,
  };
  return delays;
}

function getAudioRow(currentAudioNumber) {
  const audioRows = getAudioRows();
  return audioRows[currentAudioNumber];
}

function getMoreButton(audioRow) {
  selectAudio(audioRow);
  const moreButton = audioRow.querySelector(".audio_row__action_more");
  return moreButton;
}

function addAudioToPlaylist(audioRow) {
  const addToPlaylistActionsWindow = audioRow.querySelector(
    ".audio_row__more_actions"
  );
  const allAddToPlaylistActions = addToPlaylistActionsWindow.querySelectorAll(
    ".audio_row__more_action"
  );
  const requiredPlaylistIndex = 4;
  const requiredPlaylistButton = allAddToPlaylistActions[requiredPlaylistIndex];
  requiredPlaylistButton.click();
}

function collapseAddToPlaylistWindow(audioRow) {
  const openedAddToPlaylistActionsWindow = audioRow.querySelector(".eltt");
  openedAddToPlaylistActionsWindow.style.display = "none";
}

function isProcessEnded(amountOfAudioLeft) {
  return amountOfAudioLeft == 0;
}

function showEndedProcessNotifier(amountOfAudioToMove) {
  const endedProcessNotifier = "Треков обработано: " + (amountOfAudioToMove);
  alert(endedProcessNotifier);
}

function getTimeLeftNotifier(amountOfAudioLeft, completeDelay) {
  const timeSpendingPerAudioAddition = completeDelay / 1000;
  let timeLeftInSeconds = timeSpendingPerAudioAddition * amountOfAudioLeft;
  const secondsInHour = 3600;
  const hoursLeft = Math.floor(timeLeftInSeconds / secondsInHour);
  timeLeftInSeconds -= hoursLeft * secondsInHour;
  const secondsInMinute = 60;
  const minutesLeft = Math.floor(timeLeftInSeconds / secondsInMinute);
  timeLeftInSeconds -= minutesLeft * secondsInMinute;
  const secondsLeft = Math.floor(timeLeftInSeconds);
  const timeLeftNotifier = buildTimeLeftNotifier(
    hoursLeft, minutesLeft, secondsLeft
  );
  return timeLeftNotifier;
}

function buildTimeLeftNotifier(hoursLeft, minutesLeft, secondsLeft) {
  let timeLeftNotifierBuilder = "Осталось примерно: ";
  timeLeftNotifierBuilder += (hoursLeft + " часов ");
  timeLeftNotifierBuilder += (minutesLeft + " минут ");
  timeLeftNotifierBuilder += (secondsLeft + " секунд ");
  const timeLeftNotifier = timeLeftNotifierBuilder;
  return timeLeftNotifier;
}

function getCompletionNotifier(amountOfAudioToMove, amountOfAudioProcessed) {
  const progress = (amountOfAudioProcessed / amountOfAudioToMove);
  const allPercents = 100;
  const progressPercentage = Math.floor(progress * allPercents);
  const completionNotifier = buildCompletionNotifier(
    progressPercentage, amountOfAudioToMove, amountOfAudioProcessed
  );
  return completionNotifier;
}

function buildCompletionNotifier(
  progressPercentage, amountOfAudioToMove, amountOfAudioProcessed
) {
  let completionNotifierBuilder = "Выполнено: ";
  completionNotifierBuilder += progressPercentage;
  completionNotifierBuilder += "% ";
  completionNotifierBuilder += "(";
  completionNotifierBuilder += amountOfAudioProcessed;
  completionNotifierBuilder += "/";
  completionNotifierBuilder += amountOfAudioToMove;
  completionNotifierBuilder += ")";
  const completionNotifier = completionNotifierBuilder;
  return completionNotifier;
}
